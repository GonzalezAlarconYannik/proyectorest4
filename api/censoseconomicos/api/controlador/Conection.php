<?php

class Conection {

    private $db = "censosEconomicos";
    private $sgbd = "pgsql";
    private $host = "localhost";
    private $user = "postgres";
    private $passwd = "kinnay";

    public function __construct() {
        
    }

    /*
     * =======================================================
     * ===============FUNCIONES GET===========================
     * =======================================================
     */

    // todas las entidades


    public function getEntidades($app) {

        Try {
            $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
            $sql = 'SELECT * FROM entidad';
            $stm = $db->query($sql);
            return $stm->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {

            $app->response()->status(400);
        }
    }

    //entidades espesificas

    public function getEntidadEspecifica($datos) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "SELECT nombre FROM entidad WHERE id_entidad='$datos'";
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function val_entidad($dato, $param) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "SELECT * FROM entidad WHERE $dato='$param'";
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function val_tabla($tabla, $dato, $param) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "SELECT * FROM $tabla WHERE $dato='$param'";
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    //entidades espesificas

    public function getMunicipioEspecifico($datos) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "SELECT id_entidad, nombre FROM municipio WHERE id_municipio='$datos'";
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    // todas los valores economicos 
    public function getValorEconomico() {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = 'SELECT * FROM "valor_economico"';
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    // todas los municipio 
    public function getMunicipio() {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = 'SELECT * FROM municipio ';
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getNotas() {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = 'SELECT * FROM nota';
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getIndicador() {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = 'SELECT * FROM indicador';
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    //todos los temas de nivel3

    public function getNivel3() {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = 'SELECT * FROM nivel3';
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getNivel3Especifico($datos) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "SELECT * FROM nivel3 WHERE id_nivel3='$datos'";
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getNivel2() {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = 'SELECT * FROM nivel2';
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getNivel2Especifico($datos) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "SELECT * FROM nivel2 WHERE id_nivel2='$datos'";
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getNivel1() {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = 'SELECT * FROM nivel1';
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getNivel1Especifico($datos) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "SELECT * FROM nivel1 WHERE id_nivel1='$datos'";
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    //GET mas especificos 
    //--obtiene los municipios de entidad

    public function getMunicipiosDeUnaEntidadEspecifica($datos) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "select e.nombre,m.id_municipio as id,m.nombre
                from entidad e
                inner join municipio m
                on e.id_entidad=m.id_entidad
                where e.id_entidad='$datos'";
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    //muestra las notas que tienen todos  los indicadores

    public function getNotasIndicadores($limite) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "select i.id_indicador,i.indicador,n.id_nota,n.nota
                from indicador i
                inner join nota n
                on i.id_nota=n.id_nota limit " . $limite;
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    //-- muestra las notas que tiene un indicador en especifico

    public function getNotasDeUnIndicadorEspecifico($datos) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "select i.id_indicador,i.descripcion,n.id_nota,n.descripcion
                from indicador i
                inner join nota n
                on i.id_nota=n.id_nota
                where i.id_indicador='$datos'";
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    //-- nivel-economico

    public function getNivelEconomico() {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = 'SELECT * FROM valor_economico';
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    //--
    //-- nivel-economico

    public function getUnidadMedida() {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = 'SELECT * FROM unidad_medida';
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    //--
    //-- ontiene los temas nivel3 de un tema nivel2


    public function getTemasNivel1DelNivel2($datos) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "select  n2.id_nivel2,n2.tema_nivel2,n3.id_nivel3,n3.tema_nivel2
                from nivel2 n2
                inner join nivel3 n3 on n2.id_nivel2=n3.id_nivel2
                where n2.id_nivel2='$datos'";
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    //-- obtiene los temas nivel1,nivel2 y nivel3 de un tema nivel1 y un nivel 2 en especifico

    public function getTemasNivel1DelNivel2Nivel3($datos, $datos2) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "select n1.id_nivel1,n1.tema_nivel1,n2.id_nivel2,n2.tema_nivel2,n3.id_nivel3,n3.tema_nivel2
                from nivel1 n1
                inner join nivel2 n2 on n1.id_nivel1=n2.id_nivel1
                inner join nivel3 n3 on n2.id_nivel2=n3.id_nivel2
                where n1.id_nivel1='$datos' and n2.id_nivel2='$datos2'";
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getNivelesMedidaDeUnMunicipio($datos) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "select  m.nombre as nombreMunicipio,n3.tema_nivel3,u.medida,sum(v.cantidad)
            from entidad e
            inner join municipio m on  m.id_entidad=e.id_entidad
            inner join valor_economico v on m.id_municipio=v.id_municipio
            inner join nivel3 n3 on n3.id_nivel2=v.id_nivel3
            inner join nivel2 n2 on n2.id_nivel2=n3.id_nivel3
            inner join nivel1 n1 on n1.id_nivel1=n2.id_nivel1
            inner join unidad_medida u on u.id_unidad_medida=v.id_unidad_medida
            inner join indicador i on i.id_indicador=v.id_indicador
             where u.id_unidad_medida='$datos' group by m.id_municipio,n3.id_nivel3,u.medida";
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function deleteEntidad($id) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        try {
            $sql = "DELETE FROM entidad WHERE id_entidad = '$id'";
            $stm = $db->query($sql);
        } catch (Exception $p) {
            
        }
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function deleteMunicipio($id) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "DELETE FROM municipio WHERE id_municipio = $id";
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    /*
     * =======================================================
     * ===============FIN FUNCIONES GET===========================
     * =======================================================
     */

    /*
     * =======================================================
     * ===============FUNCIONES POST===========================
     * =======================================================
     */

    public function setPostMunicipio($id_entidad, $municipio) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "INSERT INTO municipio(id_entidad,nombre)
                         VALUES ('$id_entidad','$municipio')";
        $db->exec($sql);
        return $db->lastInsertId();
    }

    public function setPostEntidad($entidad) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "INSERT INTO entidad(nombre)
                         VALUES ('$entidad')";
        $db->exec($sql);
        return $db->lastInsertId();
    }
    
    public function setPutEntidad($id,$dato) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "UPDATE entidad SET nombre = '$dato' WHERE id_entidad = '$id'";
        $db->exec($sql);
        return $db->lastInsertId();
    }
    
    
    public function obtenerUltimoRegistro($tabla,$id) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "SELECT $id FROM $tabla ORDER BY $id DESC LIMIT 1";
        $stm = $db->query($sql);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    /*
     * =======================================================
     * ===============FIN FUNCIONES POST===========================
     * =======================================================
     */


    /*
     * 
     */

    public function deleteDeleteMunicipio($id) {
        $db = new PDO("$this->sgbd:host=$this->host;dbname=$this->db", $this->user, $this->passwd);
        $sql = "DELETE FROM municipio  WHERE id_municipio = ,'$id'";
        $db->exec($sql);
        return $db->lastInsertId();
    }

    /*
     * 
     */
}

?>
